﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
    public float speed = 4.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    private Transform target;
    public Transform Target1;
    public Transform Target2;
    private Vector2 heading = Vector2.right;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Transform target;
        target = Target1;

        if (Vector2.Distance(Target1.position, transform.position) < Vector2.Distance(Target2.position, transform.position))
        {
            target = Target1;

        }
        else
        {
            target = Target2;
        }

        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }
}

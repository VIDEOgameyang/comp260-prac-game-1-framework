﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 10.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float brake = 10.0f;
    public float turnSpeed = 30.0f; // in degrees/second

    public string Hor;
    public string Ver;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 direction;
        direction.x = Input.GetAxis(Hor);
        direction.y = Input.GetAxis(Ver);
        float turn = Input.GetAxis(Hor);
        transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

        float forwards = Input.GetAxis(Ver);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
            }
        }
        // compute a vector in the up direction of length speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // scale by the maxSpeed parameter
        Vector2 velocity = new Vector2(0, speed);

        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.World);        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }
}
